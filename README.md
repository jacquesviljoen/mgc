# README #



### Merchant's Guide to the Galaxy Application ###

### Design Decisions ###
* I created a RomanSymbol and RomanNumeral class to represent the individual symbol with its letter and numeric representation. The RomanSymbol class also defines the rules of each symbol. eg. How many times it can be repeated and whether it can be subtracted from a larger symbol or not.
* The RomanNumeral class is a collection of RomanSymbols.
* I created a LineParser class that accepts the filename and the interactor instance and handles all parsing of the lines.
* The interactor instance is injected into the LineParser class to future proof the interaction between the application and the user. The LineParser class will accept an interactor that implements the UserInteractorInterface. This means that a different type of interactor can be instantiated and injected that will receive input and provide output to a user.

### Dependencies ###
* The application requires the .Net framework 4 to run.
* It was developed using Visual Studio 2013.

### Assumptions ###
* If a line from the text field ends with a roman symbol and is preceded by “is” it is the mapping of text to symbols.
* If a line ends with the word “Credits” and is preceded by a number, 
it is assumed that any other word in the sentence that is not “is” or anyone of the symbol mappings is the item being compared to credits.
* All mappings and definitions are the same independent of the case of the word.
* It is assumed that the word/number before “credits” is the number of credits.
* Questions regarding credit conversion will contain the words “Credits is”
* There will always be a space before a “?” at the end of the line.	
* The word before the “?” will be our unit.	

### How to run the application ###

* Open the MGC.sln file in Visual Studio, build and run.
* There is a sample input file in the directory called "input.txt"
* The application will prompt you to enter the location of an input file.

Jacques Viljoen
0823916993