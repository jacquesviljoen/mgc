﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MGC.CurrencyConverter;

namespace MGC
{
    class Program
    {
        static void Main(string[] args)
        {
            //create a dictionary of our roman symbols
            Dictionary<string, RomanSymbol> symbols = new Dictionary<string, RomanSymbol>();
            symbols.Add("I", new RomanSymbol("I", 1,true,1,3,1));
            symbols.Add("V", new RomanSymbol("V", 5,false,2,1,0));
            symbols.Add("X", new RomanSymbol("X", 10,true,2,3,1));
            symbols.Add("L", new RomanSymbol("L", 50,false,3,1,0));
            symbols.Add("C", new RomanSymbol("C", 100,true,3,3,1));
            symbols.Add("D", new RomanSymbol("D", 500,false,4,1,0));
            symbols.Add("M", new RomanSymbol("M", 1000,true,4,3,1));

            //create our interactor. This can be substituted for any interactor that implements the UserInterActorInterface
            ConsoleInteractor interactor = new ConsoleInteractor();

            Console.WriteLine("************** Merchant's Guide to the Galaxy ****************");

            //prompt user to specify the full path of the input file
            string filePath = interactor.PromptUser("Please specify the full path to the sample input file:",true);

            //Check if file exists
            try 
            {
                //Inject interactor so that if we ever need to use a different output method we can
                LineParser parser = new LineParser(filePath, interactor);

                //create an instance of our currency converter
                Converter converter = new Converter(symbols, parser,interactor);
                converter.convert();

            }
            catch(Exception ex)
            {
                interactor.DisplayErrorMessage(string.Format("An error has occured: \n{0}", ex.Message));
            }

            Console.WriteLine("**************************************************************");
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

       
    }
}
