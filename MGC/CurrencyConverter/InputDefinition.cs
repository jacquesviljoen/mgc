﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MGC.CurrencyConverter
{
    /// <summary>
    /// Defines a definition entry
    /// eg. glob glob Silver is 34 Credits
    /// </summary>
    class InputDefinition
    {
        public int Qty { get; set; }
        public string Unit { get; set; }
        public decimal Credits { get; set; }

        public InputDefinition(int qty, string unit, decimal credits)
        {
            Qty = qty;
            Unit = unit;
            Credits = credits;
        }

        public decimal PricePerUnit()
        {
            return Credits / Qty;
        }
        
    }
}
