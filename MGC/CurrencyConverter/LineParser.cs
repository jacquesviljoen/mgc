﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MGC.CurrencyConverter.Contracts;

namespace MGC.CurrencyConverter
{
    /// <summary>
    /// Reads the file and parses each line
    /// </summary>
    class LineParser
    {
        protected StreamReader Reader { get; set; }
        private List<InputMapping> mappings;
        private List<InputDefinition> definitions;

        private Dictionary<string, RomanSymbol> symbols; 
        private List<string> lines;
        private UserInteractorInterface interactor;

        public LineParser(string filePath, UserInteractorInterface userInteractor)
        {
            Reader = new StreamReader(filePath);
            interactor = userInteractor;

            mappings = new List<InputMapping>();
            definitions = new List<InputDefinition>();

            //create a list of the lines because it is easier to work with list.
            CreateListOfLines();
        }

        /// <summary>
        /// Process the mappings and return a populated list
        /// </summary>
        /// <param name="romanSymbols"></param>
        /// <returns></returns>
        public List<InputMapping> ProcessMappingLines(Dictionary<string, RomanSymbol> romanSymbols)
        {
            interactor.DisplayMessage("***** Processing Mapping Lines *********", true);

            symbols = romanSymbols;

            int lineCounter = 0;

            //loop through lines and create mappings from each
            foreach (string line in lines)
            {
                if (LineIsAMapping(line))
                {
                    try
                    {
                        mappings.Add(ParseMapping(line));

                    }
                    catch (Exception ex)
                    {
                        interactor.DisplayErrorMessage(ex.Message);
                    }
                    
                }

                lineCounter++;
            }

            interactor.DisplayMessage(string.Format("Created {0} mapping/s", mappings.Count), true);

            return mappings;
        }

        /// <summary>
        /// Create an InputMapping object
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private InputMapping ParseMapping(string line)
        {
            string[] words = line.Split(' ');

            //throw an exception if the line contains a symbol that is not defined
            if(!symbols.ContainsKey(words[2]))
            {
                throw new Exception(string.Format("The symbol, {0} does not exist in the Roman Symbol mappings", words[2]));
            }

            return new InputMapping(words[0], symbols[words[2]]);
        }


        public void CreateListOfLines()
        {
            lines = new List<string>();

            string line;
            while ((line = Reader.ReadLine()) != null)
            {
                lines.Add(line);   
            }

        }

        public bool LineIsAMapping(string line)
        { 
            string[] words = line.Split(' ');

            return (words.Length == 3 && words[1].Equals("is"));

        }

        /// <summary>
        /// Process all definition lists
        /// </summary>
        /// <param name="mappings"></param>
        /// <returns></returns>
        public List<InputDefinition> ProcessDefinitionLines()
        {
            interactor.DisplayMessage("***** Processing Definition Lines *********",true);

            foreach (string line in lines)
            {
                //check if this is not a question
                char lastChar = line[line.Length - 1];
                if (!lastChar.Equals('?'))
                {
                    string[] words = line.ToLower().Split(' ');
                    List<string> possibleUnits = new List<string>();

                    int counter = 0;

                    decimal credits = 0m;
                    RomanNumeral romanNumber = new RomanNumeral();

                    //the line has to be longer than or equal to 5 words in order to be a definition
                    if (words.Length >= 5)
                    {
                        foreach (string word in words)
                        {

                            InputMapping mapping = FindMapping(word);

                            //if item is a mapping we need to add it to our roman number
                            if (mapping != null)
                            {
                                romanNumber.AddSymbol(mapping.Symbol);
                                counter++;
                                continue;
                            }

                            if(decimal.TryParse(word,out credits) && words[counter + 1].Equals("credits"))
                            {
                                counter++;
                                continue;
                            }
                            if (word.Equals("credits") && (counter + 1) == words.Length)
                            {
                                string previous = words[counter - 1];
                                credits = Decimal.Parse(previous);
                                
                            }

                            //word is either the number of credits or 
                            if (!word.Equals("is") && !word.Equals("credits"))
                            {
                                possibleUnits.Add(word);
                            }

                            counter++;
                        }

                        //we should have everything we need to create our definition
                        if (possibleUnits.Count == 0)
                        {
                            throw new Exception("The system could not determine the unit in this definition line.");
                        }

                        definitions.Add(
                            new InputDefinition(romanNumber.ToInt(), string.Join(" ", possibleUnits.ToArray()), credits)
                            );

                    }


                }

              
            }

            interactor.DisplayMessage(string.Format("Created {0} definition/s", definitions.Count), true);

            return definitions;
          
        }

        /// <summary>
        /// Loop through lines and outputs answers using the interactor
        /// </summary>
        public void AnswerQuestions()
        {
            interactor.DisplayMessage("***** Answering Questions *********", true);

            foreach (string line in lines)
            {
                //check if this is not a question
                char lastChar = line[line.Length - 1];
                if (lastChar.Equals('?'))
                {
                    string[] words = line.ToLower().Split(' ');

                    //if the line is a conversion question
                    if (line.ToLower().Contains("credits is"))
                    {
                        string answer = AnswerCreditConversionQuestion(words, mappings, definitions);
                        interactor.DisplayMessage(string.Format("Q: {0}", line),false);
                        interactor.DisplayMessage(string.Format("A: {0}", answer),true);
                    }
                    //line is a mapping question
                    else
                    {
                        string answer = AnswerMappingQuestion(words);
                        interactor.DisplayMessage(string.Format("Q: {0}", line), false);
                        interactor.DisplayMessage(string.Format("A: {0}", answer), true);
                    }
                   

                }

            }
            

        }

        /// <summary>
        /// Answer mapping question, eg. how much is pish tegj glob glob ?
        /// </summary>
        /// <param name="words"></param>
        /// <returns></returns>
        public string AnswerMappingQuestion(string[] words)
        { 
            RomanNumeral romanNumber = new RomanNumeral();
            
            string answer = "";

            foreach (string word in words)
            {
                //check if there are any mappings and add it to our roman number
                InputMapping mapping = FindMapping(word);
                if (mapping != null)
                {
                    romanNumber.AddSymbol(mapping.Symbol);
                }
            }

            if (romanNumber.SymbolCount() == 0)
            {
                answer = "I have no idea what you are talking about";
            }
            else
            {
                //now we can do some calculations
                int romanInt = romanNumber.ToInt();
                answer = string.Format("{0} is {1}", ConvertRomanToMapping(romanNumber.GetSymbols()), romanInt);
            }
            
            return answer;

        }

        /// <summary>
        /// Answer a conversion questions, eg. how many Credits is glob prok Silver ?
        /// </summary>
        /// <param name="words"></param>
        /// <param name="mappings"></param>
        /// <param name="definitions"></param>
        /// <returns></returns>
        public string AnswerCreditConversionQuestion(string[] words, List<InputMapping> mappings, List<InputDefinition> definitions)
        {
            RomanNumeral romanNumber = new RomanNumeral();
            int counter = 0;
            string answer = "";
            foreach (string word in words)
            {
                //check if there are any mappings and add it to our roman number
                InputMapping mapping = FindMapping(word);
                if (mapping != null)
                {
                    romanNumber.AddSymbol(mapping.Symbol);
                }

                //if we've reached the end, the word before the ? should be our unit
                if (word.Equals("?"))
                {
                    string unit = words[counter - 1];
                    
                    //find a definition for our unit
                    InputDefinition definition = FindDefinition(unit,definitions);

                    if (definition == null)
                    {
                        throw new Exception(string.Format("No defintion exists for the unit {0}.",unit));
                    }

                    //now we actually do the calculation
                    int romanInt = romanNumber.ToInt();
                    decimal unitPrice = definition.PricePerUnit(); 
                    decimal total = romanInt * unitPrice;
                    answer = string.Format("{0} {1} is {2} Credits",ConvertRomanToMapping(romanNumber.GetSymbols()),definition.Unit,total.ToString("#.##"));

                }

               
                counter++;
            }

            //if there are no mappings in this sentence we should throw an error
            if (romanNumber.SymbolCount() == 0)
            {
                answer = "I have no idea what you are talking about";
            }

            return answer;

        }

        /// <summary>
        /// Convert the roman number to the mappings provided
        /// </summary>
        /// <param name="symbols"></param>
        /// <returns></returns>
        public string ConvertRomanToMapping(List<RomanSymbol> symbols)
        {
            string result = "";
            foreach (RomanSymbol symbol in symbols)
            {
                InputMapping found = mappings.Find(delegate(InputMapping item) {
                    return item.Symbol == symbol;
                });

                if (found != null)
                {
                    result += string.Format(" {0}",found.Text);
                }
            }

            return result.Substring(1);
        }

        /// <summary>
        /// Search for a specific definition
        /// </summary>
        /// <param name="word"></param>
        /// <param name="definitions"></param>
        /// <returns></returns>
        public InputDefinition FindDefinition(string word, List<InputDefinition> definitions)
        {
            InputDefinition found = null;
            foreach (InputDefinition definition in definitions)
            {
                if (definition.Unit.Equals(word))
                {
                    found = definition;
                    break;
                }
            }

            return found;
        }

        /// <summary>
        /// Searhc for a specific mapping
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public InputMapping FindMapping(string word)
        {
            InputMapping found = null;
            foreach (InputMapping mapping in mappings)
            {
                if (mapping.Text.Equals(word))
                {
                    found = mapping;
                    break;
                }
            }

            return found;
        }


        public bool LineIsQuestion(string line)
        {
            return line.EndsWith("?");
        }

        public int MappingsCount()
        {
            return mappings.Count;
        }

        public int DefinitionsCount()
        {
            return definitions.Count;
        }
    }
}
