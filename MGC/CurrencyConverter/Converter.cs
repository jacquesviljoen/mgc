﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MGC.CurrencyConverter.Contracts;

namespace MGC.CurrencyConverter
{
    class Converter
    {
        private Dictionary<string,RomanSymbol> symbols;
        private LineParser parser;
        private UserInteractorInterface interactor;

        public Converter(Dictionary<string, RomanSymbol> romanSymbols, LineParser fileParser,UserInteractorInterface userInteractor)
        {
            symbols = romanSymbols;
            parser = fileParser;
            interactor = userInteractor;

        }

        /// <summary>
        /// Main method that handles the conversions
        /// </summary>
        public void convert()
        {
            //parse mapping lines. eg. glob is I
            parser.ProcessMappingLines(symbols);

            //if no mappings have been created we need to display an error
            if (parser.MappingsCount() == 0)
            {
                throw new Exception("The system could not create a list of mappings. Please ensure that the input is correct.");
            }

            //parse definition lines. eg. glob glob Silver is 34 Credits
            parser.ProcessDefinitionLines();

            //if no definitions have been created we need to display an error
            if (parser.DefinitionsCount() == 0)
            {
                throw new Exception("The system could not create a list of definitions. Please ensure that the input is correct.");
            
            }

            //lat this point we should have all data and we can now answer the questions
            parser.AnswerQuestions();
        }
    }
}
