﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MGC.CurrencyConverter.Contracts
{
    interface UserInteractorInterface
    {
        /// <summary>
        /// Prompt a user for input and return 
        /// </summary>
        /// <param name="question"></param>
        /// <param name="newLine"></param>
        /// <returns></returns>
        string PromptUser(string question, bool newLine);

        /// <summary>
        /// Display an error message
        /// </summary>
        /// <param name="message"></param>
        void DisplayErrorMessage(string message);

        /// <summary>
        /// Display a general message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="newLine"></param>
        void DisplayMessage(string message, bool newLine);

    }
}
