﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MGC.CurrencyConverter
{
    /// <summary>
    /// Defines a mapping
    /// eg. glob is I
    /// </summary>
    class InputMapping
    {
        public string Text { get; set; }
        public RomanSymbol Symbol { get; set; }

        public InputMapping(string txt, RomanSymbol romanSymbol)
        {
            Text = txt;
            Symbol = romanSymbol;
        }

    }
}
