﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MGC.CurrencyConverter
{
    /// <summary>
    /// Class representing a RomanSymbol. Several operators have been
    /// overloaded to allow comparisons between roman symbols
    /// </summary>
    class RomanSymbol : IComparable<RomanSymbol>
    {
        public string Symbol { get; set; }
        public int Value { get; set; }
        public bool CanBeSubtracted { get; set; }
        public int Hierarchy { get; set; }
        public int Repeatable { get; set; }
        public int RepeatableAfterSeperator { get; set; }

        public RomanSymbol(string symbol, int value, bool canBeSubtracted, int hierarchy, int repeatable, int repeatableAfterSeperator)
        {
            Symbol = symbol;
            Value = value;
            CanBeSubtracted = canBeSubtracted;
            Hierarchy = hierarchy;
            Repeatable = repeatable;
            RepeatableAfterSeperator = repeatableAfterSeperator;
        }

        /// <summary>
        /// Compare the Value property of each symbol
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(RomanSymbol other)
        {
            return Value.CompareTo(other.Value);
        }

        public override bool Equals(object obj)
        {
            RomanSymbol compare = (RomanSymbol)obj;
            return Symbol.Equals(compare.Symbol);
        }

        public static bool operator <(RomanSymbol a, RomanSymbol b)
        {
            return a.Value < b.Value;
        }

        public static bool operator >(RomanSymbol a, RomanSymbol b)
        {
            return a.Value > b.Value;
        }

        public static bool operator ==(RomanSymbol a, RomanSymbol b)
        {
            return a.Symbol.Equals(b.Symbol);
            
        }

        public static bool operator !=(RomanSymbol a, RomanSymbol b)
        {
            return !a.Symbol.Equals(b.Symbol);
        }


    }

   
}
