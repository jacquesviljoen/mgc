﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MGC.CurrencyConverter.Contracts;

namespace MGC.CurrencyConverter
{

    /// <summary>
    /// Implementation of the UserInteractorInterface that outputs to the console
    /// </summary>
    public class ConsoleInteractor : UserInteractorInterface
    {

        public string PromptUser(string question, bool newLine)
        {
            if (newLine)
            {
                Console.WriteLine("\n");
            }

            Console.WriteLine(question);

            return Console.ReadLine();

        }

        public void DisplayMessage(string message, bool newLine)
        {
            Console.WriteLine(message);
            
            if (newLine)
            {
                Console.WriteLine("\n");
            }
            
        }


        public void DisplayErrorMessage(string message)
        {
            Console.WriteLine("\n********************** AN ERROR OCCURRED *********************");
            Console.WriteLine(message);
            Console.WriteLine("\n");
            
        }

    }
}
