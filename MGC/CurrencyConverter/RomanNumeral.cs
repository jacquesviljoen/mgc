﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MGC.CurrencyConverter
{
    /// <summary>
    /// Class that defines the RomanNumeral. It consists of a list of RomanSymbols
    /// </summary>
    class RomanNumeral
    {
        protected List<RomanSymbol> symbols;

        public RomanNumeral() {
            symbols = new List<RomanSymbol>();
        }

        /// <summary>
        /// Assign a new symbol
        /// </summary>
        /// <param name="symbol"></param>
        public void AddSymbol(RomanSymbol symbol)
        {
            //check if we are allowed to add this symbol
            if (SymbolCanBeAdded(symbol))
            {
                symbols.Add(symbol);
            }
            else
            {
                //execution has to be stopped if a roman numeral violation has occurred as this will make all expected results invalid
                throw new Exception(string.Format("The symbol,{0}, can only be repeated a maximum of {1} times, and can be repeated {2} times if it is seperated by another symbol from its previous occurrence.",symbol.Symbol, symbol.Repeatable, symbol.RepeatableAfterSeperator));
            }
        }


        /// <summary>
        /// Check if a symbol can be added to this roman numeral
        /// </summary>
        /// <param name="newSymbol"></param>
        /// <returns></returns>
        public bool SymbolCanBeAdded(RomanSymbol newSymbol)
        {
            int occurrences = 0;
            bool hasSeperator = false;
            int prevSymbol = 0;
            int counter = 0;
            
            foreach (RomanSymbol symbol in symbols)
            {
                if (symbol == newSymbol)
                {
                    occurrences++;
                }

                //check if our previous symbol is not equal to our symbol
                if (prevSymbol != 0 && symbols.ElementAt(prevSymbol) != symbol)
                {
                    hasSeperator = true;
                }

                prevSymbol = counter;
                counter++;
            }

            //if the symbol appears multiple times, we should add 1 more for this addition.
            if (occurrences > 0)
            {
                occurrences++;
            }

            //if the number of occurrences already exceed the total number allowed, throw an exception
            if ( occurrences >= (newSymbol.Repeatable + newSymbol.RepeatableAfterSeperator) && !hasSeperator)
            {
                return false;
            }
            else
            {
                return true;
            
            }

        }


        /// <summary>
        /// Combine our symbols to provide a string representation of the roman numeral
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string combined = "";
            symbols.ForEach(delegate(RomanSymbol symbol) {
                combined += symbol.Symbol;
            });

            return combined;
        }

        /// <summary>
        /// Returns a list of symbols
        /// </summary>
        /// <returns></returns>
        public List<RomanSymbol> GetSymbols()
        {
            return symbols;
        }

        public int SymbolCount()
        {
            return symbols.Count;
        }

        /// <summary>
        /// Convert the roman numeral to an integer value
        /// </summary>
        /// <returns></returns>
        public int ToInt()
        {
            int total = 0;
            int counter = 0;
            int alreadyAdded = 0;

            foreach (RomanSymbol symbol in symbols)
            { 

                //if it is the last number just add it and break
                if (symbols.Count == (counter + 1) )
                {
                    if (alreadyAdded != counter)
                    {
                        total += symbol.Value;
                    }

                    break;
                }

                //check for exception like IX
                RomanSymbol next = symbols.ElementAt(counter + 1);

                //current is smaller than next
                if (symbol < next && symbol.CanBeSubtracted && symbol.Hierarchy < next.Hierarchy)
                {
                    total += next.Value - symbol.Value;
                    alreadyAdded = counter + 1;
                }
                else if(alreadyAdded != counter || counter == 0)
                {
                    total += symbol.Value;
                }

                counter++;
            
            }

            return total;

        }

    }
}
